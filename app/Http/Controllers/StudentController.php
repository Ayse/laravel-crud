<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = student::all();
        return view("index",['students'=>$students]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $students=Student::all();
        return view("create", compact('students'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $student= new Student;
        $student->name=$request->name;
        $student->lastname=$request->lastname;
        $student->number=$request->number;
        $student->phone=$request->phone;
        $student->save();

        return redirect()->route('index');

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Student $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Student $student
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student=Student::findOrFail($id);
        return view('edit', compact('student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Student $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $student= Student::findOrFail($id);
        $student->name=$request->name;
        $student->lastname=$request->lastname;
        $student->number=$request->number;
        $student->phone=$request->phone;
        $student->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Student $student
     * @return \Illuminate\Http\Response
     */

    public function delete($id){
/*        Student::find($id)->delete();
        return redirect()->route('index');*/

    }
    public function destroy(Student $student)
    {
        $student->delete();
        return redirect()->route('articles.index')
            ->with('success','Kayıt başarıyla silindi.');
    }
}
