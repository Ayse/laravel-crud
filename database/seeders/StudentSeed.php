<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class StudentSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('students')->insert([
            'name'=>"ayşe",
            'lastname'=>"akbulut",
            'number'=>364,
            'phone'=>"55555"
        ]);
    }
}
