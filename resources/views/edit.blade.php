<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">

    <!-- Title Page-->
    <title>Au Register Forms by Colorlib</title>

    <!-- Icons font CSS-->


<body>
<div class="page-wrapper bg-gra-01 p-t-180 p-b-100 font-poppins">
    <div class="wrapper wrapper--w780">

        <div class="card card-3">
            <div class="card-heading"></div>
            <div class="card-body">
                <h2 class="title">Registration Info</h2>
                <form action="{{ route('student.update', $student->id)}}" method="POST" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                    <div class="input-group">
                        <input class="input--style-3" type="text" placeholder="Name" value="{{$student->name}}" name="name">
                    </div>
                    <div class="input-group">
                        <input class="input--style-3" type="text" placeholder="LastName" value="{{$student->lastname}}" name="lastname">
                        <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>
                    </div>
                    {{--   <div class="input-group">
                           <div class="rs-select2 js-select-simple select--no-search">
                               <select name="gender">
                                   <option disabled="disabled" selected="selected">Gender</option>
                                   <option>Male</option>
                                   <option>Female</option>
                                   <option>Other</option>
                               </select>
                               <div class="select-dropdown"></div>
                           </div>
                       </div>--}}
                    <div class="input-group">
                        <input class="input--style-3" type="number" placeholder="Number" value="{{$student->number}}" name="number">
                    </div>
                    <div class="input-group">
                        <input class="input--style-3" type="text" placeholder="Phone" value="{{$student->phone}}"name="phone">
                    </div>
                    <div class="p-t-10">
                        <button class="btn btn--pill btn--green" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->

