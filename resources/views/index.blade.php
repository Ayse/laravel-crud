{{--
@foreach($students as $student)
    <label>{{$student->name}}</label> <br>
    <label>{{$student->lastname}}</label><br>
    <label>{{$student->number}}</label><br>
    <label>{{$student->phone}}</label><br>
@endforeach
--}}
<div class="card shadow mb-4">
    <div cla="card-heade py-3"> Öğrenciler</div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>LastName</th>
                    <th>Number</th>
                    <th>Phone</th>
                    <th>İşlemler</th>
                </tr>
                </thead>
                <tbody>
                @foreach($students as $student)

                    <tr>
                        <td> {{$student->name}}</td>
                        <td> {{$student->lastname}}</td>
                        <td> {{$student->number}}</td>
                        <td> {{$student->phone}}</td>


                        <td>
                            <a href="{{route('student.edit', $student->id)}}" class="btn btn-sm btn-primary">Düzenle</a>
                        </td>
                        <td>
                            <form  action="{{ route('student.destory', $student->id)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit"
                                   class="btn btn-sm btn-primary">Sil</button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                </tbody>

            </table>
        </div>
    </div>
</div>
