<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::resource('student', 'App\Http\Controllers\StudentController');

Route::get('/index', 'App\Http\Controllers\StudentController@index')-> name('index');

Route::get('/create', 'App\Http\Controllers\StudentController@create')-> name('create');

/*Route::get('/delete/{id}', 'App\Http\Controllers\StudentController@delete')->name('delete');*/


